# Git repository group scan

A node application that scan git repository groups and return analysis data.

### How to use it

```bash
npm i
```

```bash
npm run scan-groups
```