import { readFile, readFileSync, readdirSync, existsSync } from 'fs';
import path  from 'path';
import glob  from 'glob';
import util from 'util';

let angular = null;
let angularMaterial = null;

async function getProject(folder) {
  const files = glob.sync(path.join(folder, '**/*.ts'));
  const filesMatch = [];
  await getLibsData(folder);

  files.forEach(file => {
    const fileContent = readFileSync(file, 'utf-8');
    const matches = fileContent.match(/(\.getItem|\.getMemoryItem|\.lastLocation)/g);
    if (matches) {
      console.log({
        'project:': folder.replace('.tmp\\', ''),
        'file:': file,
        'methods:': matches,
        'Angular:': angular[0],
        'Material:': angularMaterial
      })

      filesMatch.push(file);
    }
  });

  if (filesMatch.length) {
    console.log(path.basename(folder));
  }
}

function scan(folder) {
  readdirSync(folder, { withFileTypes: true })
    .filter(dir => dir.isDirectory())
    .forEach(dir => getProject(path.join(folder, dir.name)));
}

scan('.tmp');

async function getLibsData(rootDir) {
  const packageLockPath = path.resolve(rootDir, 'package-lock.json');
  const packagePath = path.resolve(rootDir, 'package.json');

  if (existsSync(packageLockPath)) {
    return await findPackages(packageLockPath)
  }

  if (existsSync(packagePath)) {
    return await findPackages(packagePath)
  }

  return 'project has no package.json or package-lock.json';
}

async function findPackages(packagePath) {
  const packageFile = await util.promisify(readFile)(packagePath);
  const packageParsed = JSON.parse(packageFile.toString());

  angular = Object.entries(packageParsed.dependencies)
    .filter(item => item[0].includes('@angular/core'))
    .map((item: any) => ({ 
      name: item[0],
      version: (item[1]).version || item[1],
     }));

  angularMaterial = Object.entries(packageParsed.dependencies)
  .filter((item: any) => item[0].includes('@angular/material'))
  .map((item: any) => ({ 
    name: item[0],
    version: (item[1]).version || item[1],
   }));

  return;
}
