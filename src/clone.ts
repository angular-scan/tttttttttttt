import axios, { AxiosRequestConfig } from 'axios';
import { existsSync, mkdirSync, readdirSync, readFileSync }  from 'fs';
import { join } from 'path';
import git  from 'simple-git/promise';
process.env['NODE_TSL_REJECT_UNAUTHORIZED'] = '0';

// const baseUrls = 'https://gitlab.com/api/v4/groups/angular-scan/projects';

const tmpFolder = './.tmp';


const apiLink = readFileSync(join(__dirname, 'apigroup.txt'), 'utf-8');
// const token = 'uEQXNmN8szxoj-yzZAqk'
const token = readFileSync(join(__dirname, 'accessToken.txt'), 'utf-8');

async function cloneRepos(url: string) {
  const options: AxiosRequestConfig = {
    method: 'GET',
    headers: { 'PRIVATE-TOKEN': token },
    url,
  }

  const results = await axios(options);

  if (!existsSync(tmpFolder)) {
    mkdirSync(tmpFolder);
  }

  results.data.forEach(item => {
    const urlToClone = item.http_url_to_repo.replace('https://', `https://gitlab-ci-token:${token}@`);

    const dirs = readdirSync('.tmp', { withFileTypes: true })
      .filter(dirent => dirent.isDirectory())
      .map(dirent => dirent.name);

    if (!dirs.includes(item.path)) {
      const cloneRepo = branchName => { 
        return git(tmpFolder).clone(urlToClone, [
          '--depth', '1',
          '--branch', branchName
        ])
      }
      cloneRepo('master')
      .then(() => console.log(`cloned ${item.http_url_to_repo}`))
      .catch(err => {
        console.log('error cloning repo', err);
      })
    }
  })
}

cloneRepos(apiLink);