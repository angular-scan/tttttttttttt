// import { promosify } from 'util';
// import { render } from 'mustache';
// import { readFile } from 'fs';
// import { dirname } from 'path';

import { search, IAnalyticsQuery } from './analytics/findComponent';
import { getRepository } from './analytics/getRepository';
import { getDeveloperData } from './analytics/getDeveloperData';

export async function runAnalysis(options: any) {

  const resultSearch = convertParams(options.prefix);
  const results = search(resultSearch, options.rootPath);
  const repository: any = await Promise.all([ getRepository(options.rootPath) ])


  return {
    analysisDateTime: new Date().toISOString(),
    developer: getDeveloperData().userInfo.username,
    // libs: null,
    repository: repository[0].url,
    results,
  };

}

function convertParams(prefix: string) {
  const parts = prefix.split(',');
  const results: IAnalyticsQuery[] = [];

  parts.forEach(element => {
      results.push({ type: 'attr', search: element });
      results.push({ type: 'tag', search: element });
  });

  return results;
}