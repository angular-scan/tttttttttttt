import { readFile, existsSync } from 'fs';
import { parse } from 'ini';
import { resolve } from 'path';
import { promisify } from 'util';

function getUrl(data: any): string {
   return data['remote "origin"'] && data['remote "origin"'];
}

export async function getRepository(rootDir: string) {
  const folder = resolve(rootDir, '.git', 'config');
  const hasFolder = existsSync(folder);

  if (hasFolder) {
    const data = await promisify(readFile)(folder)
    return getUrl(parse(data.toString()));
  }

  return '';
}
