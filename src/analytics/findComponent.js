"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.scanComponentFile = exports.findComponents = exports.search = void 0;
require("core-js/features/array/flat-map");
var tsquery_1 = require("@phenomnomnominal/tsquery");
var fs_1 = require("fs");
var glob_1 = require("glob");
var hyntax_1 = require("hyntax");
var path_1 = require("path");
var defaultQuery = [
    { type: 'tag', search: 'high' },
    { type: 'attr', search: 'high' },
];
function search(query, path) {
    if (query === void 0) { query = defaultQuery; }
    if (path === void 0) { path = './'; }
    var queries = Array.isArray(query) ? query : [query];
    return queries.flatMap(function (querySearch) { return findComponents(path, querySearch); });
}
exports.search = search;
function findComponents(path, query) {
    return glob_1.sync(path_1.join(path, '**/*.component.ts'), { ignore: '**/node_modules' })
        .flatMap(function (file) { return scanComponentFile(file, query); });
}
exports.findComponents = findComponents;
function scanComponentFile(file, query) {
    var template = getComponentTemplate(file);
    var ast = getHtmlAST(template);
    return getAllComponents(ast, query).map(formatComponents);
}
exports.scanComponentFile = scanComponentFile;
function getHtmlAST(html) {
    var tokens = hyntax_1.tokenize(html).tokens;
    var ast = hyntax_1.constructTree(tokens).ast;
    return ast.content.children || [];
}
function getComponentTemplate(file) {
    var fileContent = fs_1.readFileSync(file, 'utf-8');
    var compAST = tsquery_1.tsquery.ast(fileContent);
    var existDecorator = tsquery_1.tsquery(compAST, 'Decorator CallExpression Identifier[name=Component]')[0];
    if (!existDecorator) {
        return '';
    }
    var inlineTemplate = tsquery_1.tsquery(compAST, 'Decorator PropertyAssignment[text=/template:/]')[0];
    if (inlineTemplate) {
        return inlineTemplate.getText();
    }
    var templateFile = tsquery_1.tsquery(compAST, 'Decorator StringLiteral[text=/.html/]')[0];
    var completePath = path_1.join(path_1.dirname(file), templateFile.text);
    if (fs_1.existsSync(completePath)) {
        return fs_1.readFileSync(completePath, 'utf-8');
    }
    return '';
}
function getAllComponents(nodes, query) {
    return nodes
        .filter(function (node) {
        return node.nodeType === 'tag';
    })
        .map(function (_a) {
        var content = _a.content;
        return (__assign(__assign({}, content), { match: searchHtmlNode(content, query) }));
    })
        .flatMap(function (node) { return __spreadArrays((node.match ? [node] : []), (node.children ? getAllComponents(node.children, query) : [])); });
}
function formatComponents(json) {
    var attrs = json.attributes ? json.attributes
        .map(function (i) {
        var _a;
        return ({
            key: i.key ? i.key.content : '',
            value: (_a = i.value) === null || _a === void 0 ? void 0 : _a.content,
        });
    }) : [];
    return {
        name: json['match'],
        type: 'component',
        attrs: attrs,
    };
}
function searchHtmlNode(node, query) {
    var searchValues = query.type === 'tag' ? [node.name] :
        query.type === 'attr' && node.attributes ?
            node.attributes.map(function (a) { var _a; return (_a = a.key) === null || _a === void 0 ? void 0 : _a.content; }) : [];
    return searchValues.find(function (v) { return v === null || v === void 0 ? void 0 : v.includes(query.search); }) || '';
}
