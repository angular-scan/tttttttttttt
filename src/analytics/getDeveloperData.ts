import {
  freemem,
  homedir,
  hostname,
  platform,
  release,
  tmpdir,
  totalmem,
  type,
  uptime,
  userInfo
} from 'os';

export function getDeveloperData() {
  return {
    // freemem: freemem(),
    // homedir: homedir(),
    // hostname: hostname(),
    // platform: platform(),
    // release: release(),
    // tmpdir: tmpdir(),
    // totalmem: totalmem,
    // type: type(),
    // uptime: uptime(),
    userInfo: userInfo(),
  };
}
