"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDeveloperData = void 0;
var os_1 = require("os");
function getDeveloperData() {
    return {
        // freemem: freemem(),
        // homedir: homedir(),
        // hostname: hostname(),
        // platform: platform(),
        // release: release(),
        // tmpdir: tmpdir(),
        // totalmem: totalmem,
        // type: type(),
        // uptime: uptime(),
        userInfo: os_1.userInfo(),
    };
}
exports.getDeveloperData = getDeveloperData;
