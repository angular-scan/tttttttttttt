import 'core-js/features/array/flat-map';

import { tsquery } from '@phenomnomnominal/tsquery';
import { readFileSync, existsSync } from 'fs';
import { sync as fileSearch } from 'glob';
import { constructTree, tokenize, TreeConstructor } from 'hyntax';
import { dirname, join } from 'path';
import { StringLiteral } from 'typescript';

export interface IAnalyticsResultAttr {
  key: string;
  value?: string;
}

export interface IAnalyticsResult {
  name: string;
  type: string;
  attrs?:IAnalyticsResultAttr[];
}

export interface IAnalyticsQuery {
  type: 'tag' | 'attr';
  search: string;
}

const defaultQuery: IAnalyticsQuery[] = [
  { type: 'tag', search: 'high' },
  { type: 'attr', search: 'high' },
];

export function search(query: IAnalyticsQuery | IAnalyticsQuery[] = defaultQuery, path = './'): IAnalyticsResult[] {
  const queries = Array.isArray(query) ? query : [query];
  return queries.flatMap(querySearch => findComponents(path, querySearch))
}

export function findComponents(path: string, query: IAnalyticsQuery): IAnalyticsResult[] {
  return fileSearch(join(path, '**/*.component.ts'), { ignore: '**/node_modules' })
    .flatMap(file => scanComponentFile(file, query));
}

export function scanComponentFile(file: string, query: IAnalyticsQuery): IAnalyticsResult[] {
  const template = getComponentTemplate(file);
  const ast = getHtmlAST(template);
  return getAllComponents(ast, query).map(formatComponents)
}

function getHtmlAST(html: string) {
  const { tokens } = tokenize(html);
  const { ast } = constructTree(tokens);

  return ast.content.children || [];
}

function getComponentTemplate(file: string) {
  const fileContent = readFileSync(file, 'utf-8');
  const compAST = tsquery.ast(fileContent);

  const existDecorator = tsquery(compAST, 'Decorator CallExpression Identifier[name=Component]')[0] as StringLiteral;
  if (!existDecorator) {
    return '';
  }


  const inlineTemplate = tsquery(compAST, 'Decorator PropertyAssignment[text=/template:/]')[0] as StringLiteral;

  if (inlineTemplate) {
    return inlineTemplate.getText();
  }

  const templateFile = tsquery(compAST, 'Decorator StringLiteral[text=/.html/]')[0] as StringLiteral;
  const completePath = join(dirname(file), templateFile.text);
  if (existsSync(completePath)) {
    return readFileSync(completePath, 'utf-8');
  }

  return '';
}

function getAllComponents(nodes: TreeConstructor.AnyNode[], query: IAnalyticsQuery): TreeConstructor.NodeContents.Tag[] {
  return nodes 
    .filter(node => {
      return node.nodeType === 'tag'
    })
    .map(({ content }: TreeConstructor.Node<any, any>) => ({
      ...content,
      match: searchHtmlNode(content, query),
    }))
    .flatMap(node => [
      ...(node.match ? [node] : []),
      ...(node.children ? getAllComponents(node.children, query) : []),
    ]);
}

function formatComponents(json: TreeConstructor.NodeContents.Tag): IAnalyticsResult {
  const attrs = json.attributes ? json.attributes
    .map(i => ({
      key: i.key ? i.key.content : '',
      value: i.value?.content,
    })) : [];

    return {
      name: (json as any)['match'],
      type: 'component',
      attrs,
    };
}

function searchHtmlNode(node: TreeConstructor.NodeContents.Tag, query: IAnalyticsQuery) {
  const searchValues = query.type === 'tag' ? [node.name] :
    query.type === 'attr' && node.attributes ?
      node.attributes.map(a => a.key?.content) : [];

  return searchValues.find(v => v?.includes(query.search)) || '';
}
