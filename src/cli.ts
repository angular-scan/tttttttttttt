import { Command } from 'commander';
import { writeFile } from 'fs';
import { exec } from 'shelljs';
import { prompt } from 'inquirer';

const program = new Command();
const write = (url: any, content: any) => {
  return new Promise((resolve, reject) => {
    writeFile(url, content, function(err) {
      if (err) {
        reject(err)
      }

      resolve();
    });
  });
}

program
    .command('apiLink [link]')
    .description('Cria arquivo de link')
    .action(async (apiLink) => {
      let answers;

      if (!apiLink) {

         answers = await prompt([
          {
              type: 'input',
              name: 'link',
              message: 'Qual é o link da api do git?'
          },
          {
            type: 'input',
            name: 'token',
            message: 'Qual é seu access token?'
        }
      ]);
      }

        await Promise.all([ 
          write('./apigroup.txt', apiLink ?? answers.link),
          write('./accessToken.txt', answers.token),
        ]);

        exec('npm run scan-group', { silent: false });

    });

program.parse(process.argv);