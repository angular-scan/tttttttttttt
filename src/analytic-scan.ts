
import { tsquery } from '@phenomnomnominal/tsquery';
import { readFileSync, existsSync } from 'fs';
import { sync as fileSearch } from 'glob';
import { constructTree, tokenize, TreeConstructor } from 'hyntax';
import { dirname, join } from 'path';
import { StringLiteral } from 'typescript';
import { runAnalysis } from './analytics';
import { getRepository } from './analytics/getRepository';
const fs = require(`fs`);


async function getProject(folder) {
  const results = await runAnalysis({
    rootPath: folder,
    prefix: 'apollo,high'
  });
  // results.repository = results.repository.replace(/gitlab-ci-token:(.+)@/g, '');

  console.log(results);
}

function scan(folder) {
  fs.readdirSync(folder, { withFileTypes: true })
    .filter(dir => dir.isDirectory())
    .forEach(dir => getProject(join(folder, dir.name)))
}

scan('.tmp');
